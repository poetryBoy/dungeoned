package game.dungeoned.tower.data.shop;

import game.dungeoned.tower.data.config.ConfigFactory;
import game.dungeoned.tower.data.config.JsonConfigFactory;

public class ShopFactory extends JsonConfigFactory<String,Shop>{

    public static ShopFactory get(){
        return ConfigFactory.getFactory(ShopFactory.class);
    }

    @Override
    protected String getFilePath() {
        return "shops.json";
    }

    @Override
    protected String getKey(Shop shop) {
        return shop.getShopId();
    }

    @Override
    protected Class<Shop> getConfigClass() {
        return Shop.class;
    }
}
