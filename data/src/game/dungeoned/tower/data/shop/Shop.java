package game.dungeoned.tower.data.shop;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.archiving.Attribute;
import lombok.Data;

@Data
public class Shop {

    private String shopId;
    /** 首次消耗的金币 */
    private int firstCost;
    /** 每次购买属性加成 */
    private Attribute attribute;

    /**
     * 商店购买属性
     */
    public boolean buy(Archiving archiving, Atag atag){
        int shopNum = archiving.getShopNum(shopId);
        int cost = getCost(shopNum);
        if(cost > archiving.getGold()){
            return false;
        }
        int add = attribute.getAttr(atag);
        archiving.getAttribute().addAttr(atag,add);
        archiving.addShopNum(shopId);
        archiving.addGold(-cost);
        archiving.getDataListener().notifyPropertyView();
        return true;
    }

    /**
     * 获取本次的金币消耗
     * @param num 购买次数
     * @return 消耗
     */
    public int getCost(int num){
        int augment = num * (firstCost /2 + 1);
        return firstCost + augment;
    }
}
