package game.dungeoned.tower.data;

import game.dungeoned.tower.data.archiving.Pos;

public interface DataListener {
    /**
     * 通知属性面板刷新
     */
    void notifyPropertyView();
    /**
     * 通知地图刷新
     */
    void notifyMapScreen();
    /**
     * 移动完成后
     * @param newPos 新坐标
     */
    void afterMove(Pos newPos);
    /**
     * 地图场景切换
     */
    void changeMap();
    /**
     * 打开商店
     * @param shopId 商店id
     */
    void openShop(String shopId);



}
