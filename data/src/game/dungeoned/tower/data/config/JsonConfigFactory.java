package game.dungeoned.tower.data.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class JsonConfigFactory<K,V> extends ConfigFactory<K,V>{

    private final static String configDir = "config/";

    protected JSONArray readJsonFile(String fileName){
        URL resource = JsonConfigFactory.class.getClassLoader().getResource(configDir + fileName);
        File file = new File(resource.getPath());
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            StringBuilder stringBuilder = new StringBuilder();
            bufferedReader.lines().forEach(stringBuilder::append);
            return JSONArray.parseArray(stringBuilder.toString());
        }catch (Exception e){
            e.printStackTrace();
            System.out.printf("config is NULL : %s",fileName);
            return null;
        }
    }

    @Override
    protected Map<K, V> initConfig() {
        JSONArray array = readJsonFile(getFilePath());
        if(array == null){
            return Collections.emptyMap();
        }
        Map<K,V> kvMap = new HashMap<>();
        for (Object value : array) {
            JSONObject next = (JSONObject) value;
            V v = next.toJavaObject(getConfigClass());
            kvMap.put(getKey(v),v);
        }
        return kvMap;
    }
}
