package game.dungeoned.tower.data.config;

import java.util.Map;

/**
 * 配置工厂
 */
public interface ICfgFactory<K,V> {

    V getConfig(K key);

    Map<K,V> getConfigs();
}
