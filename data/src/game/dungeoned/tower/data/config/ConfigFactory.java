package game.dungeoned.tower.data.config;

import java.util.HashMap;
import java.util.Map;

public abstract class ConfigFactory<K,V> implements ICfgFactory<K,V> {
    private final static Map<Class<?>,ConfigFactory<?,?>> FACTORY_MAP = new HashMap<>();

    private final Map<K,V> configMap;

    public ConfigFactory(){
        configMap = initConfig();
        FACTORY_MAP.put(this.getClass(),this);
    }

    @Override
    public V getConfig(K key) {
        return configMap.get(key);
    }

    @Override
    public Map<K, V> getConfigs() {
        return configMap;
    }

    protected abstract Map<K,V> initConfig();

    protected abstract String getFilePath();

    protected abstract K getKey(V v);

    protected abstract Class<V> getConfigClass();

    public static <T extends ConfigFactory<?,?>> T getFactory(Class<T> clazz){
        return (T) FACTORY_MAP.computeIfAbsent(clazz, c -> {
            try {
                return (T) c.newInstance();
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        });
    }
}
