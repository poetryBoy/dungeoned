package game.dungeoned.tower.data.archiving;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Attribute{

    private Map<Atag,Integer> attrMap = new HashMap<>();

    @JSONField(serialize = false)
    public int getAttr(Atag atag){
        return attrMap.getOrDefault(atag,0);
    }
    @JSONField(serialize = false)
    public void setAttr(Atag atag,int value){
        attrMap.put(atag,value);
    }
    public void addAttr(Atag atag,int add){
        int attr = getAttr(atag);
        attr += add;
        if(atag == Atag.HP && attr > getAttr(Atag.HPMAX)){
            attr = getAttr(Atag.HPMAX);
        }
        attrMap.put(atag,attr);
    }

    public Attribute change(int hp,int hpmax,int atk,int def){
        addAttr(Atag.HPMAX,hpmax);
        addAttr(Atag.HP,hp);
        addAttr(Atag.ATK,atk);
        addAttr(Atag.DEF,def);
        return this;
    }

    public void change(Map<Atag,Integer> attrMap){
        for (Map.Entry<Atag, Integer> entry : attrMap.entrySet()) {
            addAttr(entry.getKey(),entry.getValue());
        }
    }

    public void change(Attribute attribute){
        change(attribute.getAttrMap());
    }


    public Attribute fill(Attribute attribute){
        this.attrMap.putAll(attribute.getAttrMap());
        return this;
    }

    public static void main(String[] args) {
        Attribute change = new Attribute().change(1, 1, 1, 1);
        String s = JSON.toJSONString(change);
        System.out.println(s);
    }

}
