package game.dungeoned.tower.data.archiving;

public enum Atag {
    HP("血量"),    //当前血量
    HPMAX("血量max"), //最大血量
    ATK("攻击力"),   //攻击力
    DEF("防御力"), //防御力
    ;

    private final String name;

    Atag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

