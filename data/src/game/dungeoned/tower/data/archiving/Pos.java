package game.dungeoned.tower.data.archiving;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pos {
    private int x;
    private int y;
}
