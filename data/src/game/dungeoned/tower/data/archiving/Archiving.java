package game.dungeoned.tower.data.archiving;

import com.alibaba.fastjson.annotation.JSONField;
import game.dungeoned.tower.data.DataListener;
import game.dungeoned.tower.data.map.MapModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Archiving {
    /** 存档Id */
    private int archivingId;
    /** 当前所处塔层 */
    private int towerId;
    /** 等级 */
    private int level;
    /** 金币 */
    private int gold;
    /** 当前坐标位置 */
    private Pos pos;
    /** 钥匙数量 */
    private Map<Key,Integer> keyMap;
    /** 基础属性 */
    private Attribute attribute;
    /** 商店次数使用 */
    private Map<String,Integer> shopNumMap;
    /** 地图数据 */
    private Map<Integer, MapModel> mapModelMap;
    /** 数据监听回调 */
    @JSONField(serialize = false)
    private DataListener dataListener;
    /** 修改时间 */
    private Instant updateTime;

    @JSONField(serialize = false)
    public int getKeyNum(Key key){
        return keyMap.getOrDefault(key,0);
    }

    public void addKey(Key key){
        int keyNum = getKeyNum(key);
        keyMap.put(key,keyNum + 1);
    }

    public void addGold(int add){
        this.gold += add;
    }

    @JSONField(serialize = false)
    public int getShopNum(String shopId){
        return shopNumMap.getOrDefault(shopId,0);
    }

    public void addShopNum(String shopId){
        shopNumMap.put(shopId,getShopNum(shopId) + 1);
    }

}
