package game.dungeoned.tower.data;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Pos;
import game.dungeoned.tower.data.map.impl.MoveResult;

import java.util.Map;

/**
 * 数据持有对象
 */
public interface DataHolder {
    /**
     * 选择存档的数据
     * 不存在则创建
     */
    void chooseArchiving(int archivingId);
    /**
     * 获取当前存档数据
     */
    Archiving getArchiving();
    /**
     * 保存存档
     */
    void saveArchiving();
    /**
     * 删除存档
     */
    void deleteArchiving(int archivingId);

    /**
     * 移动
     */
    MoveResult moveOn(Pos pos);

    /**
     * 获取所有存档
     * @return 所有
     */
    Map<Integer,Archiving> getArchivingMap();

}
