package game.dungeoned.tower.data.map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import game.dungeoned.tower.data.config.ConfigFactory;
import game.dungeoned.tower.data.config.JsonConfigFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapFactory extends JsonConfigFactory<Integer,MapModel>{
    /** 网格边长 */
    public static int gridSideLen = 10;

    public static MapFactory get(){
        return ConfigFactory.getFactory(MapFactory.class);
    }

    @Override
    protected Map<Integer, MapModel> initConfig() {
        Map<Integer,MapModel> mapConfigs = new HashMap<>();
        try {
            JSONArray array = readJsonFile(getFilePath());
            if(array == null){
                return Collections.emptyMap();
            }
            for (Object value : array) {
                JSONObject next = (JSONObject) value;
                int id = next.getInteger("id");
                Element[][] elements = new Element[gridSideLen][gridSideLen];
                JSONArray elementArray = next.getJSONArray("elements");
                for (int x = 0; x < elementArray.size(); x++) {
                    JSONArray indexArray  = (JSONArray) elementArray.get(x);
                    for (int y = 0; y < indexArray.size(); y++) {
                        JSONArray valueArray = (JSONArray) indexArray.get(y);
                        if(valueArray.isEmpty()){
                            continue;
                        }
                        String type = valueArray.getString(0);
                        if(type == null || type.equals("")){
                            continue;
                        }
                        ElementType elementType = Enum.valueOf(ElementType.class, type.toUpperCase());
                        Element element = elementType.buildElement(valueArray);
                        elements[x][y] = element;
                    }
                }
                MapModel mapModel = new MapModel(id, elements);
                mapConfigs.put(mapModel.getTowerId(),mapModel);
            }
        }catch (Exception e){
            System.out.println("场景地图配置加载失败");
            e.printStackTrace();
        }
        return mapConfigs;
    }

    @Override
    protected String getFilePath() {
        return "towerMap.json";
    }

    @Override
    protected Integer getKey(MapModel mapModel) {
        return mapModel.getTowerId();
    }

    @Override
    protected Class<MapModel> getConfigClass() {
        return MapModel.class;
    }
}
