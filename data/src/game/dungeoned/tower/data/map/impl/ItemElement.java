package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.item.Item;
import game.dungeoned.tower.data.item.ItemFactory;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import lombok.Data;

@Data
public class ItemElement implements Element {

    private final String itemId;

    @Override
    public String id() {
        return itemId;
    }

    @Override
    public ElementType type() {
        return ElementType.ITEM;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        Item item = ItemFactory.get().getConfig(itemId);
        if(item == null){
            return MoveResult.fail("item[" + itemId + "] not exits");
        }
        if(item.getAttribute() != null){
            archiving.getAttribute().change(item.getAttribute());
        }
        if(item.getKey() != null){
            archiving.addKey(item.getKey());
        }
        if(item.getGold() > 0){
            archiving.setGold(archiving.getGold() + item.getGold());
        }
        return MoveResult.ok();
    }

    @Override
    public boolean needRefreshProperty() {
        return true;
    }
}
