package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.battle.Battle;
import game.dungeoned.tower.data.battle.BattleResult;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.monster.Monster;
import game.dungeoned.tower.data.monster.MonsterFactory;
import lombok.Data;

@Data
public class MonsterElement implements Element {
    /** 怪物id */
    private final String id;

    @Override
    public String id() {
        return id;
    }

    @Override
    public ElementType type() {
        return ElementType.MONSTER;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        Monster monster = MonsterFactory.get().getConfig(id);
        if(monster == null){
            return MoveResult.fail("monster[" + id + "] not exist");
        }
        BattleResult battleResult = Battle.INS.battle(archiving.getAttribute(), monster.getAttribute());
        if(battleResult.isResult()){
            archiving.addGold(monster.getGold());
        }
        return battleResult.isResult() ? MoveResult.ok() : MoveResult.fail("战斗失败");
    }

    @Override
    public boolean needRefreshProperty() {
        return true;
    }

    @Override
    public boolean needAnimation() {
        return true;
    }
}
