package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Pos;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StairsElement implements Element {

    private String param;
    private int towerId;
    private Pos pos;

    public StairsElement(String param){
        this.param = param;
        String[] split = param.split("-");
        if(split.length != 3){
            throw new RuntimeException("stairs config error:" + param);
        }
        this.towerId = Integer.parseInt(split[0]);
        this.pos = new Pos(Integer.parseInt(split[1]),Integer.parseInt(split[2]));
    }

    @Override
    public String id() {
        return "stairs";
    }

    @Override
    public ElementType type() {
        return ElementType.STAIRS;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        return MoveResult.fail(null);
    }

    @Override
    public void afterAction(Archiving archiving) {
        //切换地图场景
        archiving.setTowerId(towerId);
        archiving.setPos(pos);
        archiving.getDataListener().changeMap();
    }

    @Override
    public boolean needRefreshProperty() {
        return false;
    }

    @Override
    public boolean needAnimation() {
        return false;
    }
}
