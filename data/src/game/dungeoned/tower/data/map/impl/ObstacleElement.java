package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import lombok.Data;

@Data
public class ObstacleElement implements Element {

    private final String id;

    @Override
    public String id() {
        return id;
    }

    @Override
    public ElementType type() {
        return ElementType.OBSTACLE;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        return MoveResult.fail(null);
    }

}
