package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.archiving.Archiving;
import lombok.Data;

@Data
public class ShopElement implements Element {

    private final String shopId;

    @Override
    public String id() {
        return shopId;
    }

    @Override
    public ElementType type() {
        return ElementType.SHOP;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        return MoveResult.fail(null);
    }

    @Override
    public void afterAction(Archiving archiving) {
        archiving.getDataListener().openShop(shopId);
    }
}
