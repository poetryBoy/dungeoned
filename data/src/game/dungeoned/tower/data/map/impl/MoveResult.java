package game.dungeoned.tower.data.map.impl;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MoveResult {

    private boolean result;

    private String msg;

    public static MoveResult ok(){
        return new MoveResult(true,"");
    }

    public static MoveResult fail(String msg){
        return new MoveResult(false,msg);
    }


}
