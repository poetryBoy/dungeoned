package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.archiving.Archiving;
import lombok.Data;

@Data
public class WallElement implements Element {

    @Override
    public String id() {
        return "wall";
    }

    @Override
    public ElementType type() {
        return ElementType.WALL;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        return MoveResult.fail(null);
    }

}
