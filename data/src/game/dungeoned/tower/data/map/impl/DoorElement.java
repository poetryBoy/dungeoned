package game.dungeoned.tower.data.map.impl;

import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Key;
import lombok.Data;

@Data
public class DoorElement implements Element {

    private final Key key;

    @Override
    public String id() {
        return "door_" + key.name().toLowerCase();
    }

    @Override
    public ElementType type() {
        return ElementType.DOOR;
    }

    @Override
    public MoveResult doAction(Archiving archiving) {
        int num = archiving.getKeyNum(key);
        if(num <= 0){
            return MoveResult.fail("钥匙数量不足");
        }
        num -= 1;
        archiving.getKeyMap().put(key,num);
        return MoveResult.ok();
    }

    @Override
    public boolean needRefreshProperty() {
        return true;
    }

}
