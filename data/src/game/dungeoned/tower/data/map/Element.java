package game.dungeoned.tower.data.map;

import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.map.impl.MoveResult;

public interface Element {
    /**
     * 元素Id,用于关联渲染的资源或者其他
     * @return String
     */
    String id();
    /**
     * 元素类型
     * @return ElementType
     */
    ElementType type();
    /**
     * 移动到元素上的操作
     * @param archiving 数据
     * @return 结果
     */
    MoveResult doAction(Archiving archiving);

    /**
     * 之后
     * @param archiving 数据
     */
    default void afterAction(Archiving archiving){//do nothing
    }

    /**
     * 是否需要刷新属性面板
     */
    default boolean needRefreshProperty(){return false;}

    /**
     * 是否需要动画展示
     */
    default boolean needAnimation(){return false;}



}
