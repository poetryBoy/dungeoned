package game.dungeoned.tower.data.map;

import com.alibaba.fastjson.JSONArray;
import game.dungeoned.tower.data.map.impl.*;
import game.dungeoned.tower.data.archiving.Key;

public enum ElementType{
    WALL{ //墙壁
        @Override
        public Element buildElement(JSONArray array) {
            return new WallElement();
        }
    },
    MONSTER { //怪物
        @Override
        public Element buildElement(JSONArray array) {
            return new MonsterElement(array.getString(1));
        }
    },
    DOOR{  //上锁的门
        @Override
        public Element buildElement(JSONArray array) {
            String key = array.getString(1).toUpperCase();
            Key keyEnum = Enum.valueOf(Key.class, key);
            return new DoorElement(keyEnum);
        }
    },
    SHOP{ //商店
        @Override
        public Element buildElement(JSONArray array) {
            return new ShopElement(array.getString(1));
        }
    },
    ITEM{   //道具
        @Override
        public Element buildElement(JSONArray array) {
            return new ItemElement(array.getString(1));
        }
    },
    STAIRS{  //楼梯
        @Override
        public Element buildElement(JSONArray array) {
            return new StairsElement(array.getString(1));
        }
    },
    OBSTACLE{//障碍
        @Override
        public Element buildElement(JSONArray array) {
            return new ObstacleElement(array.getString(1));
        }
    },
    ;

    public abstract Element buildElement(JSONArray array);
}
