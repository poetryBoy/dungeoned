package game.dungeoned.tower.data.map;

import lombok.Data;

/**
 * 地图（塔）数据
 * 单一层
 */
@Data
public class MapModel {
    /** 塔层数 */
    private final int towerId;
    /** 网格元素数据 */
    private final Element[][] elements;
}
