package game.dungeoned.tower.data.holder;

import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.data.DataListener;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Attribute;
import game.dungeoned.tower.data.archiving.Pos;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.MapFactory;
import game.dungeoned.tower.data.map.MapModel;
import game.dungeoned.tower.data.map.impl.MoveResult;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据持有 抽象实现
 */
public abstract class AbstractDataHolder implements DataHolder {

    private Archiving archiving;
    private final DataListener dataListener;
    private final Map<Integer, Archiving> archivingMap;

    public AbstractDataHolder(DataListener dataListener) {
        this.dataListener = dataListener;
        archivingMap = initArchivingMap();
    }

    @Override
    public void chooseArchiving(int archivingId) {
        Archiving archiving = archivingMap.get(archivingId);
        if(archiving == null){
            archiving = new Archiving(archivingId,1,0,0,new Pos(1,1),new HashMap<>(),
                    new Attribute().change(100,100,10,5),new HashMap<>(),MapFactory.get().getConfigs(),dataListener,Instant.now());
            archivingMap.put(archivingId,archiving);
            persistence(archiving);
        }
        if(archiving.getDataListener() == null){
            archiving.setDataListener(dataListener);
        }
        this.archiving = archiving;
        dataListener.notifyPropertyView();
        dataListener.notifyMapScreen();
    }

    @Override
    public Archiving getArchiving() {
        return archiving;
    }

    @Override
    public void saveArchiving() {
        if(archiving != null){
            archiving.setUpdateTime(Instant.now());
            persistence(archiving);
        }
    }

    @Override
    public void deleteArchiving(int archivingId) {
        Archiving remove = archivingMap.remove(archivingId);
        if(remove != null) persistence(archivingMap.values());
    }

    @Override
    public Map<Integer, Archiving> getArchivingMap() {
        return archivingMap;
    }

    @Override
    public MoveResult moveOn(Pos pos) {
        MapModel mapModel = this.archiving.getMapModelMap().get(archiving.getTowerId());
        Element element = mapModel.getElements()[pos.getX()][pos.getY()];
        MoveResult moveResult = element == null ? MoveResult.ok() : element.doAction(archiving);
        if(moveResult.isResult()){
            archiving.setPos(pos);
            mapModel.getElements()[pos.getX()][pos.getY()] = null;
            dataListener.afterMove(pos);
            if(element != null && element.needRefreshProperty()){
                dataListener.notifyPropertyView();
            }
        }
        if(element != null) element.afterAction(archiving);
        return moveResult;
    }

    /**
     * 持久化数据对象
     */
    protected abstract void persistence(Archiving archiving);
    protected abstract void persistence(Collection<Archiving> archivings);

    /**
     * 获取所有的持久化存档
     */
    protected abstract Map<Integer,Archiving> initArchivingMap();
}
