package game.dungeoned.tower.data.item;

import game.dungeoned.tower.data.archiving.Attribute;
import game.dungeoned.tower.data.archiving.Key;
import lombok.Data;

@Data
public class Item {
    /** id */
    private String itemId;
    /** 物品名称 */
    private String name;
    /** 属性加成 */
    private Attribute attribute;
    /** 钥匙 */
    private Key key;
    /** 金币 */
    private int gold;

}
