package game.dungeoned.tower.data.item;

import game.dungeoned.tower.data.config.ConfigFactory;
import game.dungeoned.tower.data.config.JsonConfigFactory;

public class ItemFactory extends JsonConfigFactory<String,Item>{

    public static ItemFactory get(){
        return ConfigFactory.getFactory(ItemFactory.class);
    }

    @Override
    protected String getFilePath() {
        return "items.json";
    }

    @Override
    protected String getKey(Item item) {
        return item.getItemId();
    }

    @Override
    protected Class<Item> getConfigClass() {
        return Item.class;
    }
}
