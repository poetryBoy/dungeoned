package game.dungeoned.tower.data.battle;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BattleResult {
    /** 结果 */
    private boolean result;
    /** hp消耗 */
    private int hpCost;

}
