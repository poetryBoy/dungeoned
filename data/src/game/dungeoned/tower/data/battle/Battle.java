package game.dungeoned.tower.data.battle;

import game.dungeoned.tower.data.archiving.Attribute;

public interface Battle {

    Battle INS = new SimpleBattle();
    /**
     * 战斗结算
     * @param attribute1 发起者属性
     * @param attribute2 被挑战者属性
     * @return 是否胜利
     */
    BattleResult battle(Attribute attribute1,Attribute attribute2);
}
