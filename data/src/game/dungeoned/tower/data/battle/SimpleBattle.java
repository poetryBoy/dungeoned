package game.dungeoned.tower.data.battle;

import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.archiving.Attribute;

/**
 * 简单战斗
 * 攻击方先手，各自造成(自身攻击力-对方防御力)的伤害（未破防默认1点伤害）,直到一方死亡
 * 为了计算所需血量消耗，这里直到被挑战者死亡才停止结算
 */
public class SimpleBattle implements Battle{
    @Override
    public BattleResult battle(Attribute attribute1, Attribute attribute2) {
        int demage1 = attribute1.getAttr(Atag.ATK) - attribute2.getAttr(Atag.DEF) > 0
                ? attribute1.getAttr(Atag.ATK) - attribute2.getAttr(Atag.DEF) : 1;
        int demage2 = attribute2.getAttr(Atag.ATK) - attribute1.getAttr(Atag.DEF) > 0
                ? attribute2.getAttr(Atag.ATK) - attribute1.getAttr(Atag.DEF) : 1;
        int hpCost = 0;
        int hp2 = attribute2.getAttr(Atag.HP);
        while (true){
            hp2 -= demage1;
            if(hp2 <= 0){
                break;
            }
            hpCost += demage2;
        }
        BattleResult battleResult = new BattleResult(hpCost < attribute1.getAttr(Atag.HP), hpCost);
        if(battleResult.isResult()) attribute1.addAttr(Atag.HP,-hpCost);
        return battleResult;
    }
}
