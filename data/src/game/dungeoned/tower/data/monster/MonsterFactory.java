package game.dungeoned.tower.data.monster;

import game.dungeoned.tower.data.config.ConfigFactory;
import game.dungeoned.tower.data.config.JsonConfigFactory;

public class MonsterFactory extends JsonConfigFactory<String,Monster>{

    public static MonsterFactory get(){
        return ConfigFactory.getFactory(MonsterFactory.class);
    }

    @Override
    protected String getFilePath() {
        return "monsters.json";
    }

    @Override
    protected String getKey(Monster monster) {
        return monster.getMonsterId();
    }

    @Override
    protected Class<Monster> getConfigClass() {
        return Monster.class;
    }
}
