package game.dungeoned.tower.data.monster;

import game.dungeoned.tower.data.archiving.Attribute;
import lombok.Data;

@Data
public class Monster {
    /** 怪物id */
    private String monsterId;
    /** 怪物名称 */
    private String name;
    /** 基础属性 */
    private Attribute attribute;
    /** 金币 */
    private int gold;
}
