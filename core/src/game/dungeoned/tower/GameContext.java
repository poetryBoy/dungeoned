package game.dungeoned.tower;

/**
 * 游戏上下文
 */
public class GameContext {
    //场景操作器
    private static ScreenHandle screenHandle;

    public static void setScreenHandle(ScreenHandle screenHandle){
        GameContext.screenHandle = screenHandle;
    }

    public static ScreenHandle getScreenHandle() {
        if(screenHandle == null){
            throw new NullPointerException("ScreenHandle is NULL");
        }
        return screenHandle;
    }

    public static void destroy(){
        GameContext.screenHandle = null;
    }
}
