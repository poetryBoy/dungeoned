package game.dungeoned.tower.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutorManager {

    private static final Map<String, ExecutorService> EXECUTER_MAP = new HashMap<>();

    public static final ExecutorConfig DEFAULT_CONFIG;

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        DEFAULT_CONFIG = new ExecutorConfig(
                availableProcessors, availableProcessors * 2, 5,
                TimeUnit.MINUTES
        );
    }

    public static ExecutorService getExecutor(String type,ExecutorConfig config){
        if(config.threadFactory == null)
            config.threadFactory = new ThreadFactory() {
                private final AtomicInteger atomicInteger = new AtomicInteger(1);
                private final String namePrefix = "exec-" + type.toLowerCase() + "-%d";

                @Override
                public Thread newThread(Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setDaemon(false);
                    thread.setName(String.format(namePrefix,atomicInteger.getAndIncrement()));
                    return thread;
                }
            };
        if(config.workQueue == null){
            config.workQueue = new ArrayBlockingQueue<>(5);
        }
        return EXECUTER_MAP.computeIfAbsent(type, t->{
            if(config.rejectedExecutionHandler == null){
                return new ThreadPoolExecutor(config.corePoolSize,config.maximumPoolSize,
                        config.keepAliveTime,config.timeUnit,
                        config.workQueue,config.threadFactory);
            }else{
                return new ThreadPoolExecutor(config.corePoolSize,config.maximumPoolSize,
                        config.keepAliveTime,config.timeUnit,
                        config.workQueue,config.threadFactory,config.rejectedExecutionHandler);
            }
        });
    }

    public static ExecutorService getExecutor(Type type,ExecutorConfig config){
        return getExecutor(type.name(),config);
    }

    /**
     * 获取默认的线程池
     */
    public static ExecutorService getdefualtExecutor(){
        return getExecutor(Type.DEFAULT,DEFAULT_CONFIG);
    }

    public static void destory(){
        for (ExecutorService executorService : EXECUTER_MAP.values()) {
            if(!executorService.isShutdown()){
                executorService.shutdown();
            }
        }
    }




    public enum Type{
        DEFAULT
    }

    public static class ExecutorConfig{
        //核心线程数
        public int corePoolSize;
        //线程池最大线程数
        public int maximumPoolSize;
        //线程存活时间
        public int keepAliveTime;
        //时间单位
        public TimeUnit timeUnit;
        //阻塞任务队列
        public BlockingQueue<Runnable> workQueue;
        //线程工厂
        public ThreadFactory threadFactory;
        //拒绝策略
        public RejectedExecutionHandler rejectedExecutionHandler;

        public ExecutorConfig(int corePoolSize, int maximumPoolSize, int keepAliveTime,
                              TimeUnit timeUnit, BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
            this.corePoolSize = corePoolSize;
            this.maximumPoolSize = maximumPoolSize;
            this.keepAliveTime = keepAliveTime;
            this.timeUnit = timeUnit;
            this.workQueue = workQueue;
            this.threadFactory = threadFactory;
            this.rejectedExecutionHandler = rejectedExecutionHandler;
        }

        public ExecutorConfig(int corePoolSize, int maximumPoolSize, int keepAliveTime, TimeUnit timeUnit) {
            this.corePoolSize = corePoolSize;
            this.maximumPoolSize = maximumPoolSize;
            this.keepAliveTime = keepAliveTime;
            this.timeUnit = timeUnit;
        }
    }

}
