package game.dungeoned.tower.utils;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

public class ActionUtil {

    private ActionUtil(){}


    public static Action flickerActionByAlpha(float time,float...alphas){
        return flickerActionByAlpha(time,-1,alphas);
    }

    /**
     * 通过操作透明度实现闪烁的效果,依赖背景会有不同的效果
     * @param time 每次动作的时间
     * @param num 循环多少次 -1为永久
     * @param alphas 闪烁的透明度变化
     * @return 动作
     */
    public static Action flickerActionByAlpha(float time,int num,float...alphas){
        SequenceAction sequenceAction = Actions.sequence();
        for (float alpha : alphas) {
            AlphaAction alphaAction = Actions.alpha(alpha, time);
            sequenceAction.addAction(alphaAction);
        }
        return Actions.repeat(num,sequenceAction);
    }

}
