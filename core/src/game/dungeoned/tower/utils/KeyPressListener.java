package game.dungeoned.tower.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class KeyPressListener extends InputListener {

    private static final long delay = 450L; //触发长按的延迟时间
    private static final long interval = 70L; //间隔时间

    private final Set<KeyTime> downSet = new CopyOnWriteArraySet<>();
    private final Consumer<Integer> consumer;

    public KeyPressListener(Consumer<Integer> consumer){
        this.consumer = consumer;
        ExecutorService executorService = ExecutorManager.getdefualtExecutor();
        executorService.execute(() -> {
            while (true){
                if(downSet.isEmpty()){
                    continue;
                }
                long now = System.currentTimeMillis();
                for (KeyTime keyTime : downSet) {
                    //满足长按条件
                    long end = now - keyTime.time;
                    if(keyTime.press || end >= delay){
                        keyTime.press = true;
                        if(end >= interval){
                            consumer.accept(keyTime.keyCode);
                            keyTime.time = now;
                        }
                    }
                }
                try {
                    Thread.sleep(Float.valueOf(Gdx.graphics.getDeltaTime()).longValue() * 1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        consumer.accept(keycode);
        downSet.add(new KeyTime(keycode));
        return true;
    }

    @Override
    public boolean keyUp(InputEvent event, int keycode) {
        downSet.remove(new KeyTime(keycode));
        return true;
    }

    public void clear(){
        this.downSet.clear();
    }

    private static class KeyTime{
        public int keyCode;
        public long time;
        public boolean press = false;

        public KeyTime(int keyCode, long time) {
            this.keyCode = keyCode;
            this.time = time;
        }

        public KeyTime(int keyCode){
            this(keyCode,System.currentTimeMillis());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            KeyTime keyTime = (KeyTime) o;
            return keyCode == keyTime.keyCode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(keyCode);
        }
    }
}
