package game.dungeoned.tower.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 图片分割工具
 */
public class ImageSpliter {

    public static void main(String[] args) {
        String imagePath = "C:\\Users\\user\\Desktop\\gdxres\\tower_pack.jpg";
        String outputPath = "C:\\Users\\user\\Desktop\\gdxres\\tower_altas";
        String outputType = "png";
        split(imagePath,outputPath,outputType,30,30);
    }

    public static void split(String imagePath,String outputPath,String outputType,int row,int col){
        File file = new File(imagePath);
        if(!file.exists()){
            throw new RuntimeException("image path not exist:" + imagePath);
        }
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            int widthParent = bufferedImage.getWidth();
            int heightParent = bufferedImage.getHeight();
            int width = widthParent / row;
            int height = heightParent / col;
            if(width % row != 0){
                width += 1;
                height += 1;
                widthParent = width * row;
                heightParent = height * col;
                BufferedImage newImage = new BufferedImage(widthParent,heightParent,bufferedImage.getType());
                Graphics2D graphics = newImage.createGraphics();
                graphics.drawImage(bufferedImage,0,0,null);
                graphics.dispose();
                bufferedImage = newImage;
            }
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    BufferedImage childImage = new BufferedImage(width,height,bufferedImage.getType());
                    Graphics2D graphics = childImage.createGraphics();
                    graphics.drawImage(bufferedImage.getSubimage(i * width,j * height,width,height),0,0,null);
                    graphics.dispose();
                    File imageOutput = new File(outputPath,i + "-" +  j + "." + outputType);
                    File parentFile = imageOutput.getParentFile();
                    if(!parentFile.exists()){
                        parentFile.mkdir();
                    }
                    ImageIO.write(childImage,outputType,imageOutput);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
