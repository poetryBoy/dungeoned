package game.dungeoned.tower.res;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

import java.util.Map;

public class ResManager{

    private static AssetManager assetManager;

    public static void init(){
        if(assetManager != null){
            assetManager.dispose();
            assetManager = null;
        }
        Gdx.app.log("asset","load asset res ...");
        assetManager = new AssetManager();
        for (Res res : Res.values()) {
            assetManager.load(res.getPath(),res.getBindClass());
        }
        assetManager.finishLoading();
        Gdx.app.log("asset","load asset finish!");
    }

    public static  <T> T getRes(Res res){
        return assetManager.get(res.getPath());
    }

    public static <T> T getResIfAbsent(String resPath, Class<T> tClass){
        T t = assetManager.get(resPath,false);
        if(t == null){
            assetManager.load(resPath,tClass);
            t = assetManager.finishLoadingAsset(resPath);
        }
        if(t == null){
            Gdx.app.error("asset","load asset error:" + resPath);
        }
        return t;
    }

    public static void dispose() {
        if(ResManager.assetManager != null){
            ResManager.assetManager.dispose();
        }
    }

}
