package game.dungeoned.tower.res;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * 资源枚举
 */
public enum Res{
    //开始界面背景图
    START_BACKGROUND("start_background.jpeg",Texture.class),
    //自定义字体
    CUSTOM_FONT("custom.fnt", BitmapFont.class),
    //血量数字字体
    HP_COST("hp_cost.fnt", BitmapFont.class),
    //按钮初始图片
    BUTTON_ON("button/Button_White_on.png",Texture.class),
    //按钮按下图片
    BUTTON_DOWN("button/Button_White_down.png",Texture.class),
    //血量图标
    HP("hp.png",Texture.class),
    //血量上限图标
    HP_MAX("hp_max.png",Texture.class),
    //攻击力图标
    ATK("atk.png",Texture.class),
    //防御力图标
    DEF("def.png",Texture.class),
    //金币图标
    GOLD("gold.png",Texture.class),
    //红钥匙
    RED_KEY("item/key_red.png",Texture.class),
    //蓝钥匙
    BLUE_KEY("item/key_blue.png",Texture.class),
    //黄钥匙
    YELLOW_KEY("item/key_yellow.png",Texture.class),
    //主角
    ROLE("role.png",Texture.class),
    //弹窗背景
    WINDOW_WHITE("window_white.png",Texture.class),
    //删除按钮
    DELETE("button/delete.png",Texture.class),
    //label背景
    LABEL_BK("label_white.png",Texture.class),
    ;
    private final String resPath;
    private final Class<?> bindClass;

    Res(String resPath, Class<?> bindClass) {
        this.resPath = resPath;
        this.bindClass = bindClass;
    }

    public String getPath() {
        return resPath;
    }

    public Class<?> getBindClass() {
        return bindClass;
    }

    public <T> T get(){
        return ResManager.getRes(this);
    }

    public static Res byPath(String path){
        for (Res value : Res.values()) {
            if(value.resPath.equals(path)){
                return value;
            }
        }
        return null;
    }
}
