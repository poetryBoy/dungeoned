package game.dungeoned.tower;

public interface Constants {
    //世界宽度
    float MAIN_WIDTH = 600f;
    //世界高度
    float MAIN_HEIGHT = 300f;
    //地图格子边长
    int MAP_SIDE_GRID_LEN = 10;
    //存档个数
    int ARCHVING_NUM = 3;

    interface LeftPanel{
        //塔层模板
        String TEMF = "%s层";
        //标签间隔填充
        float LABEL_PAD = 10f;
        //图标边长大小
        float IMG_SIZE = 17f;

    }


}
