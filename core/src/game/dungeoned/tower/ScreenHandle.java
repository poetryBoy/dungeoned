package game.dungeoned.tower;

import game.dungeoned.tower.data.DataHolder;

/**
 * 场景操作
 */
public interface ScreenHandle {

    /**
     * 获取游戏界面宽度
     * @return 宽度
     */
    float getMainWidth();

    /**
     * 获取游戏界面高度
     * @return 高度
     */
    float getMainHeight();

    /**
     * 显示开始场景界面
     */
    void showStartScreen();

    /**
     * 显示游戏场景界面
     */
    void showGameScreen();

    /**
     * 获取数据持有对象
     */
    DataHolder getDataHolder();


}
