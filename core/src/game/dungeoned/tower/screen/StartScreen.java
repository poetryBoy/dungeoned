package game.dungeoned.tower.screen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import game.dungeoned.tower.group.StartButtonGroup;
import game.dungeoned.tower.res.Res;

public class StartScreen extends BaseScreen {

    public StartScreen() {
        //背景图
        Texture texture = Res.START_BACKGROUND.get();
        Image background = new Image(texture);
        background.setBounds(0,0,stage.getWidth(),stage.getHeight());
        background.getColor().a = 0.9f; //设置透明度
        stage.addActor(background);
        StartButtonGroup startButtonGroup = new StartButtonGroup();
        stage.addActor(startButtonGroup);
    }
}
