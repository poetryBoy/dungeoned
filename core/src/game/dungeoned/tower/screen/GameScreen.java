package game.dungeoned.tower.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.dungeoned.tower.actor.ShopWindow;
import game.dungeoned.tower.data.DataListener;
import game.dungeoned.tower.data.archiving.Pos;
import game.dungeoned.tower.group.GameLeftGroup;
import game.dungeoned.tower.group.GameMiddleGroup;
import game.dungeoned.tower.res.Res;
import game.dungeoned.tower.utils.KeyPressListener;

public class GameScreen extends BaseScreen {

    private final GameLeftGroup gameLeftGroup;
    private final GameMiddleGroup gameMiddleGroup;
    private final KeyPressListener keyPressListener;
    private final ShopWindow shopWindow;

    public GameScreen() {
        gameLeftGroup = new GameLeftGroup(stage.getWidth()/4,stage.getHeight());
        gameLeftGroup.setPosition(0,0);
        stage.addActor(gameLeftGroup);

        gameMiddleGroup = new GameMiddleGroup(stage.getWidth()/2);
        gameMiddleGroup.setPosition(gameLeftGroup.getX() + gameLeftGroup.getWidth(),0);
        stage.addActor(gameMiddleGroup);
        keyPressListener = new KeyPressListener(gameMiddleGroup::moveAction);
        stage.addListener(keyPressListener);
        Window.WindowStyle windowStyle = new Window.WindowStyle();
        windowStyle.titleFont = Res.CUSTOM_FONT.get();
        Texture texture = Res.WINDOW_WHITE.get();
        windowStyle.background = new Image(texture).getDrawable();
        shopWindow = new ShopWindow("",windowStyle,stage.getWidth()/3,
                stage.getHeight());
        shopWindow.setPosition(stage.getWidth()/2 - shopWindow.getWidth()/2,
                stage.getHeight()/2 - shopWindow.getHeight()/2);
    }

    @Override
    protected Color clearColor() {
        return Color.TAN;
    }



    public DataListener createListener(){
        return new GameDatalistener();
    }
    public class GameDatalistener implements DataListener{

        @Override
        public void notifyPropertyView() {
            gameLeftGroup.syncDataView();
            gameMiddleGroup.reloadHpCost();
        }

        @Override
        public void notifyMapScreen() {
            gameMiddleGroup.syncAllMapData();
        }

        @Override
        public void afterMove(Pos newPos) {
            gameMiddleGroup.syncData(newPos);
        }

        @Override
        public void changeMap() {
            //切换场景效果
            RunnableAction begin = Actions.run(() -> Gdx.input.setInputProcessor(null));
            RunnableAction end = Actions.run(() -> Gdx.input.setInputProcessor(stage));
            RunnableAction listenerClear = Actions.run(keyPressListener::clear);
            AlphaAction alpha1 = Actions.alpha(0.0f, 0.4f);
            AlphaAction alpha2 = Actions.alpha(0.7f, 0.4f);
            RunnableAction notify = Actions.run(()->{
                notifyPropertyView();
                notifyMapScreen();
            });
            AlphaAction alpha3 = Actions.alpha(1.0f, 0.3f);
            SequenceAction sequence = Actions.sequence(
                    begin,
                    listenerClear,
                    alpha1, alpha2, notify,alpha3,
                    end);
            gameMiddleGroup.addAction(sequence);
        }

        @Override
        public void openShop(String shopId) {
            shopWindow.show(shopId,stage);
            stage.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    shopWindow.hide();
                    stage.removeListener(this);
                }
            });
        }
    }
}
