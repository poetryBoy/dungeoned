package game.dungeoned.tower.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import game.dungeoned.tower.GameContext;

public class BaseScreen extends ScreenAdapter {

    protected final Stage stage;

    public BaseScreen() {
        stage = new Stage(new StretchViewport(
                GameContext.getScreenHandle().getMainWidth(),GameContext.getScreenHandle().getMainHeight()));
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(clearColor());
        stage.act();
        stage.draw();
    }

    public void show(){
        Gdx.input.setInputProcessor(stage);
    }

    protected Color clearColor(){
        return Color.WHITE;
    }

    @Override
    public void dispose() {
        if(stage != null){
            stage.dispose();
        }
    }

}
