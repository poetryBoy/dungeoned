package game.dungeoned.tower;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.holder.LocalDataHolder;
import game.dungeoned.tower.data.map.MapFactory;
import game.dungeoned.tower.res.ResManager;
import game.dungeoned.tower.screen.GameScreen;
import game.dungeoned.tower.screen.StartScreen;
import game.dungeoned.tower.utils.ExecutorManager;

public class Dungeoned extends Game implements ScreenHandle{

	private float mainWidth;  //宽
	private float mainHeight; //高

	private StartScreen startScreen;
	private GameScreen gameScreen;
	private DataHolder dataHolder;

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		this.mainWidth = Constants.MAIN_WIDTH;
		this.mainHeight = Constants.MAIN_HEIGHT;
		//资源管理初始化
		ResManager.init();
		//全局上下文
		GameContext.setScreenHandle(this);
		startScreen = new StartScreen();
		gameScreen = new GameScreen();
		showStartScreen();
		//数据逻辑管理初始化
		MapFactory.gridSideLen = Constants.MAP_SIDE_GRID_LEN;
		dataHolder = new LocalDataHolder(gameScreen.createListener());
	}

	@Override
	public void dispose () {
		GameContext.destroy();
		ResManager.dispose();
		if(startScreen != null){
			startScreen.dispose();
		}
		if(gameScreen != null){
			gameScreen.dispose();
		}
		if(dataHolder != null){
			dataHolder.saveArchiving();
		}
		ExecutorManager.destory();
	}

	@Override
	public float getMainWidth() {
		return mainWidth;
	}

	@Override
	public float getMainHeight() {
		return mainHeight;
	}

	@Override
	public void showStartScreen() {
		startScreen.show();
		this.setScreen(startScreen);
	}

	@Override
	public void showGameScreen() {
		gameScreen.show();
		this.setScreen(gameScreen);
	}

	@Override
	public DataHolder getDataHolder() {
		return dataHolder;
	}
}
