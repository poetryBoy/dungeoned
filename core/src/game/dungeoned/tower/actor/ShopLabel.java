package game.dungeoned.tower.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.shop.Shop;
import game.dungeoned.tower.res.Res;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class ShopLabel extends Group {
    private final Atag atag;
    private final Label label;
    private ShopClickListener clickListener;

    public ShopLabel(Atag atag,float width,float height) {
        this.atag = atag;
        setSize(width,height);
        Texture texture = Res.LABEL_BK.get();
        Image background = new Image(texture);
        background.setBounds(0,0,getWidth(),getHeight());
        addActor(background);
        //通用style考虑使用pool管理
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Res.CUSTOM_FONT.get();
        style.fontColor = Color.BLACK;
        label = new Label("无",style);
        label.setFontScale(0.4f);
        label.setSize(label.getPrefWidth(),label.getPrefHeight());
        addActor(label);
    }

    public void setShop(Shop shop,Supplier<Void> supplier){
        int num = shop.getAttribute().getAttr(atag);
        label.setText(String.format(atag.getName() + " + %s",num));
        label.setWidth(label.getPrefWidth());
        label.setPosition(getWidth()/2 - label.getWidth()/2,getHeight()/2 - label.getHeight()/2);
        this.setOrigin(Align.center);
        if(clickListener == null){
            clickListener = new ShopClickListener(shop) {
                @Override
                public void clickOver() {
                    supplier.get();
                }
            };
            addListener(clickListener);
        }
        clickListener.setShop(shop);
    }

    public abstract class ShopClickListener extends ClickListener{
        private Shop shop;

        public ShopClickListener(Shop shop){
            this.shop = shop;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            if(shop != null){
                DataHolder dataHolder = GameContext.getScreenHandle().getDataHolder();
                Archiving archiving = dataHolder.getArchiving();
                boolean buy = shop.buy(archiving, atag);
                if(buy){
                    Gdx.app.debug("shopbuy","buy " + atag.getName());
                    clickOver();
                }else{
                    Gdx.app.error("shopbuy","gold is not enough");
                }
            }
        }

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            ScaleToAction scaleToAction = Actions.scaleTo(0.8f, 0.8f);
            addAction(scaleToAction);
            event.cancel();
            return super.touchDown(event,x,y,pointer,button);
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            ScaleToAction scaleToAction = Actions.scaleTo(1.0f, 1.0f);
            addAction(scaleToAction);
            super.touchUp(event,x,y,pointer,button);
        }

        public abstract void clickOver();

        public void setShop(Shop shop){
            this.shop = shop;
        }
    }
}
