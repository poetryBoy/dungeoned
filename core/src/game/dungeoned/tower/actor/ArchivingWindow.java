package game.dungeoned.tower.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.dungeoned.tower.Constants;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.ScreenHandle;
import game.dungeoned.tower.data.archiving.Archiving;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ArchivingWindow extends BaseWindow {

    private static final String TEXT_TOWERID = "当前层数:  %s层";
    private static final String TEXT_TIME = "时间: %s";
    private final Map<Integer, ArchivingLabel> archvingLabelMap = new HashMap<>();

    public ArchivingWindow(String title, WindowStyle windowStyle) {
        super(title, windowStyle);
        ScreenHandle screenHandle = GameContext.getScreenHandle();
        setSize(screenHandle.getMainWidth()/2,screenHandle.getMainHeight() * 0.75f);
        float subHeight = getHeight() / Constants.ARCHVING_NUM;
        for (int i = 1; i <= Constants.ARCHVING_NUM; i++) {
            ArchivingLabel archivingLabel = new ArchivingLabel(i,getWidth(),subHeight);
            archivingLabel.setPosition(0,getHeight() - i * archivingLabel.getHeight());
            archivingLabel.setOrigin(Align.center);
            archivingLabel.addListener(new ClickListener(Input.Buttons.LEFT){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    ScaleToAction scaleToAction = Actions.scaleTo(0.9f, 0.9f);
                    archivingLabel.addAction(scaleToAction);
                    Gdx.app.debug("choose","choose archiving");
                    screenHandle.getDataHolder().chooseArchiving(archivingLabel.getId());
                    screenHandle.showGameScreen();
                }

                @Override
                public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    ScaleToAction scaleToAction = Actions.scaleTo(1.1f, 1.1f);
                    archivingLabel.addAction(scaleToAction);
                }

                @Override
                public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                    ScaleToAction scaleToAction = Actions.scaleTo(1.0f, 1.0f);
                    archivingLabel.addAction(scaleToAction);
                }
            });
            //右键删除
            archivingLabel.addListener(new ClickListener(Input.Buttons.RIGHT){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    screenHandle.getDataHolder().deleteArchiving(archivingLabel.getId());
                    archivingLabel.setText();
                }
            });
            archvingLabelMap.put(i, archivingLabel);
            add(archivingLabel).colspan(2);
            if(i != Constants.ARCHVING_NUM){
                row();
            }
        }
    }

    @Override
    public void preShow(){
        Map<Integer, Archiving> archivingMap = GameContext.getScreenHandle().getDataHolder().getArchivingMap();
        for (Archiving archiving : archivingMap.values()) {
            ArchivingLabel archivingLabel = archvingLabelMap.get(archiving.getArchivingId());
            if(archivingLabel == null){
                continue;
            }
            LocalDateTime localDateTime = LocalDateTime.ofInstant(archiving.getUpdateTime(), ZoneId.systemDefault());
            String date = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            archivingLabel.setText(String.format(TEXT_TOWERID,archiving.getTowerId()),
                    String.format(TEXT_TIME,date));
        }
    }
}
