package game.dungeoned.tower.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import game.dungeoned.tower.res.Res;

public class ArchivingLabel extends Group {
    private final int id;
    private final Label label1;
    private final Label label2;


    public ArchivingLabel(int id, float width, float height){
        setSize(width,height);
        this.id = id;
        Texture texture = Res.LABEL_BK.get();
        Image background = new Image(texture);
        background.setBounds(0,0,getWidth(),getHeight());
        addActor(background);
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Res.CUSTOM_FONT.get();
        style.fontColor = Color.BROWN;
        label1 = new Label("",style);
        label1.setSize(label1.getPrefWidth(),label1.getPrefHeight());
        addActor(label1);
        label2 = new Label("",style);
        label2.setSize(label2.getPrefWidth(),label2.getPrefHeight());
        addActor(label2);
        setText();
    }

    public void setText(String text1,String text2){
        label1.setText(text1);
        label1.setWidth(label1.getPrefWidth());
        label1.setPosition(getWidth()/2 - label1.getWidth()/2,getHeight() * 0.70f - label1.getHeight()/2);
        label2.setText(text2);
        label2.setWidth(label2.getPrefWidth());
        label2.setPosition(getWidth()/2 - label2.getWidth()/2,getHeight() * 0.25f - label2.getHeight()/2);
    }

    public void setText(){
        setText("无","无");
    }

    public int getId() {
        return id;
    }
}
