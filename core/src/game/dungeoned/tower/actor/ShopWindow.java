package game.dungeoned.tower.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.shop.Shop;
import game.dungeoned.tower.data.shop.ShopFactory;
import game.dungeoned.tower.res.Res;

import java.util.ArrayList;
import java.util.List;


public class ShopWindow extends BaseWindow{

    private Shop shop;
    private final Label priceLabel;
    private final List<ShopLabel> shopLabelList;

    public ShopWindow(String title, WindowStyle style,float width,float height) {
        super(title, style);
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Res.CUSTOM_FONT.get();
        labelStyle.fontColor = Color.GOLDENROD;
        priceLabel = new Label("无",labelStyle);
        add(priceLabel);
        row();
        shopLabelList = new ArrayList<>();
        float subHeight = (height-priceLabel.getHeight()) / 6;
        for (Atag atag : Atag.values()) {
            ShopLabel shopLabel = new ShopLabel(atag,width,subHeight);
            shopLabelList.add(shopLabel);
            add(shopLabel);
            row();
        }
        setSize(width,getPrefHeight());
    }

    public void show(String shopId, Stage stage){
        if(this.shop == null || !this.shop.getShopId().equals(shopId)){
            Shop shop = ShopFactory.get().getConfig(shopId);
            if(shop == null){
                Gdx.app.error("shop","shop[" + shopId + "] not exits");
                return;
            }
            this.shop = shop;
            for (ShopLabel shopLabel : shopLabelList) {
                shopLabel.setShop(shop, () -> {
                    preShow();
                    return null;
                });
            }
        }
        super.show(stage.getRoot());
    }

    @Override
    protected void preShow() {
        if(shop == null){
            return;
        }
        DataHolder dataHolder = GameContext.getScreenHandle().getDataHolder();
        Archiving archiving = dataHolder.getArchiving();
        int cost = shop.getCost(archiving.getShopNum(shop.getShopId()));
        priceLabel.setText(String.format("price: %s",cost));
        priceLabel.setWidth(priceLabel.getPrefWidth());
        priceLabel.setPosition(getWidth()/2 - priceLabel.getWidth()/2,getHeight() - 15);
    }

}
