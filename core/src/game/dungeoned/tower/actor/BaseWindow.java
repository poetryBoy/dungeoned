package game.dungeoned.tower.actor;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

public class BaseWindow extends Window {

    public BaseWindow(String title, Skin skin) {
        super(title, skin);
    }

    public BaseWindow(String title, Skin skin, String styleName) {
        super(title, skin, styleName);
    }

    public BaseWindow(String title, WindowStyle style) {
        super(title, style);
    }

    /**
     * 显示到指定舞台上
     */
    public final void show(Group group){
        preShow();
        group.addActor(this);
        postShow();
    }

    /**
     * 隐藏
     */
    public final void hide(){
        preHide();
        Group parent = getParent();
        if(parent != null){
            parent.removeActor(this);
        }
        postHide();
    }

    protected void preShow(){}

    protected void postShow(){}

    protected void preHide(){}

    protected void postHide(){}

}
