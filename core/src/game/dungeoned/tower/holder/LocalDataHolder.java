package game.dungeoned.tower.holder;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import game.dungeoned.tower.data.DataListener;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.holder.AbstractDataHolder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class LocalDataHolder extends AbstractDataHolder {
    private final String key = "archiving_";
    private Preferences preferences;

    public LocalDataHolder(DataListener dataListener) {
        super(dataListener);
    }

    @Override
    protected void persistence(Archiving archiving) {
        preferences.putString(key + archiving.getArchivingId(),
                JSONObject.toJSONString(archiving, SerializerFeature.WriteClassName));
        preferences.flush();
    }

    @Override
    protected void persistence(Collection<Archiving> archivings) {
        //先清空，后全覆盖修改
        preferences.clear();
        for (Archiving archiving : archivings) {
            preferences.putString(key + archiving.getArchivingId(),
                    JSONObject.toJSONString(archiving, SerializerFeature.WriteClassName));
        }
        preferences.flush();
    }

    @Override
    protected Map<Integer, Archiving> initArchivingMap() {
        if(preferences == null){
            preferences = Gdx.app.getPreferences("local_data");
        }
        Map<String, ?> stringMap = preferences.get();
        Map<Integer,Archiving> dataMap = new HashMap<>();
        for (Object value : stringMap.values()) {
            Archiving archiving = (Archiving) JSONObject.parse(value.toString(), Feature.SupportAutoType);
            dataMap.put(archiving.getArchivingId(),archiving);
        }
        return dataMap;
    }
}
