package game.dungeoned.tower.group;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.actor.ArchivingWindow;
import game.dungeoned.tower.res.Res;
import game.dungeoned.tower.utils.ActionUtil;

public class StartButtonGroup extends BaseGroup {

    private final TextButton startButton;  //开始
    private final TextButton optionButton; //选项
    private final TextButton exitButton;   //退出

    public StartButtonGroup() {
        super(GameContext.getScreenHandle().getMainWidth(),GameContext.getScreenHandle().getMainHeight());
        //按钮样式
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = Res.CUSTOM_FONT.get();
        textButtonStyle.font.getData().padLeft = 4;
        textButtonStyle.font.getData().setScale(0.6f);
        textButtonStyle.fontColor = Color.BROWN;
        textButtonStyle.focusedFontColor = Color.ORANGE;
        textButtonStyle.downFontColor = Color.GOLDENROD;
        textButtonStyle.overFontColor = Color.GOLD;
        Texture onTexture = Res.BUTTON_ON.get();
        textButtonStyle.up = new TextureRegionDrawable(onTexture);
        Texture downTexture = Res.BUTTON_DOWN.get();
        textButtonStyle.down = new TextureRegionDrawable(downTexture);

        Window.WindowStyle windowStyle = new Window.WindowStyle();
        windowStyle.titleFont = Res.CUSTOM_FONT.get();
        Texture texture = Res.WINDOW_WHITE.get();
        windowStyle.background = new Image(texture).getDrawable();
        ArchivingWindow archivingWindow = new ArchivingWindow("",windowStyle);
        archivingWindow.setPosition(getWidth()/2 - archivingWindow.getWidth()/2,
                getHeight()/2 - archivingWindow.getHeight()/2);


        //按钮宽高
        float width = GameContext.getScreenHandle().getMainWidth() / 5;
        float height = width / 5;

        //开始按钮
        startButton = new TextButton("开始",textButtonStyle);
        startButton.setBounds(
                GameContext.getScreenHandle().getMainWidth()/2 - width/2,
                GameContext.getScreenHandle().getMainHeight()/3 - height/2,
                width,height);
        //设置闪烁动作
        startButton.addAction(ActionUtil.flickerActionByAlpha(0.35f,0.9f,0.6f,0.3f,0.6f));
        startButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                archivingWindow.show(getStage().getRoot());
                addListener(new ClickListener(){
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        archivingWindow.hide();
                        removeListener(this);
                    }
                });
            }
        });
        this.addActor(startButton);
        //选项按钮
        optionButton = new TextButton("选项",textButtonStyle);
        optionButton.setBounds(
                startButton.getX(),
                 startButton.getY() - height,
                width,height);
        optionButton.getColor().a = 0.75f;
        this.addActor(optionButton);
        //退出按钮
        exitButton = new TextButton("退出",textButtonStyle);
        exitButton.setBounds(
                startButton.getX(),
                optionButton.getY() - height,
                width,height);
        exitButton.getColor().a = 0.75f;
        //添加点击事件 退出游戏
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        this.addActor(exitButton);
    }

    public TextButton getStartButton() {
        return startButton;
    }

    public TextButton getOptionButton() {
        return optionButton;
    }

    public TextButton getExitButton() {
        return exitButton;
    }
}
