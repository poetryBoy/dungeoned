package game.dungeoned.tower.group;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.archiving.Attribute;
import game.dungeoned.tower.data.battle.Battle;
import game.dungeoned.tower.data.battle.BattleResult;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.monster.Monster;
import game.dungeoned.tower.data.monster.MonsterFactory;
import game.dungeoned.tower.res.Res;
import game.dungeoned.tower.res.ResManager;

public class ElementGroup extends BaseGroup {

    private static final String suffix = ".png";
    private static final String anima_suffix = "_a";

    private final String id;
    //血量消耗
    private Label hpCostLabel;
    //动画效果
    private Animation<Texture> animation;
    //状态时间
    private float stateTime;

    public ElementGroup(Element element, float len){
        super(len,len);
        this.id = element.id();
        String prefix = element.type().name().toLowerCase() + "/";
        if(element.needAnimation()){
            Texture texture1 = ResManager.getResIfAbsent(prefix + id + anima_suffix + "_1" + suffix,Texture.class);
            Texture texture2 = ResManager.getResIfAbsent(prefix + id + anima_suffix + "_2" + suffix,Texture.class);
            animation = new Animation<>(0.4f,texture1,texture2);
            animation.setPlayMode(Animation.PlayMode.LOOP);
        }else{
            Texture texture = ResManager.getResIfAbsent(prefix + id + suffix,Texture.class);
            Image image = new Image(texture);
            image.setSize(len,len);
            addActor(image);
        }
        //怪物新增右下角血量消耗显示
        if(element.type() == ElementType.MONSTER){
            BitmapFont bitmapFont = Res.HP_COST.get();
            Label.LabelStyle style = new Label.LabelStyle(bitmapFont,null);
            hpCostLabel = new Label("0",style);
            hpCostLabel.setFontScale(0.4f);
            hpCostLabel.setSize(hpCostLabel.getPrefWidth(),hpCostLabel.getPrefHeight());
            hpCostLabel.setPosition(getWidth() - hpCostLabel.getPrefWidth(),0);
            addActor(hpCostLabel);
        }
    }

    /**
     * 重新加载血量消耗
     */
    public void reloadHpCost(){
        if(hpCostLabel == null){
            return;
        }
        Archiving archiving = GameContext.getScreenHandle().getDataHolder().getArchiving();
        Attribute attribute = archiving.getAttribute();
        Monster monster = MonsterFactory.get().getConfig(id);
        Battle battle = Battle.INS;
        int hp = attribute.getAttr(Atag.HP);
        Attribute attribute_ = new Attribute().fill(attribute);
        BattleResult result = battle.battle(attribute_, monster.getAttribute());
        int hpCost = result.getHpCost();
        hpCostLabel.setText(hpCost);
        if(hpCost >= 0){
            hpCostLabel.getStyle().fontColor = Color.WHITE;
        }
        /*if(hpCost >= (hp / 10)){
            hpCostLabel.getStyle().fontColor = Color.SKY;
        }
        if(hpCost >= (hp / 5)){
            hpCostLabel.getStyle().fontColor = Color.BROWN;
        }
        if(hpCost >= (hp / 2)){
            hpCostLabel.getStyle().fontColor = Color.FIREBRICK;
        }
        if(hpCost >= hp){
            hpCostLabel.getStyle().fontColor = Color.RED;
        }*/
        hpCostLabel.setWidth(hpCostLabel.getPrefWidth());
        hpCostLabel.setX(getWidth() - hpCostLabel.getPrefWidth());
    }




    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(animation != null){
            stateTime += Gdx.graphics.getDeltaTime();
            Texture texture = animation.getKeyFrame(stateTime);
            batch.draw(texture,this.getX(),this.getY());
        }
        super.draw(batch, parentAlpha);
    }
}
