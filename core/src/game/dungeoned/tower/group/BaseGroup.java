package game.dungeoned.tower.group;

import com.badlogic.gdx.scenes.scene2d.Group;

public class BaseGroup extends Group {

    public BaseGroup(float width,float height){
        setSize(width,height);
    }

}
