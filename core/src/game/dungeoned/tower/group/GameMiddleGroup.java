package game.dungeoned.tower.group;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import game.dungeoned.tower.Constants;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.actor.ShopWindow;
import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Pos;
import game.dungeoned.tower.data.map.Element;
import game.dungeoned.tower.data.map.ElementType;
import game.dungeoned.tower.data.map.MapModel;
import game.dungeoned.tower.data.map.impl.MoveResult;
import game.dungeoned.tower.res.Res;

public class GameMiddleGroup extends BaseGroup {
    /** 地图元素 */
    private ElementGroup[][] elementGroups;
    /** 人物 */
    private final Image role;
    //单个元素边长
    private final float singleLen;


    public GameMiddleGroup(float len){
        super(len,len);
        singleLen = getWidth() / Constants.MAP_SIDE_GRID_LEN;
        elementGroups = new ElementGroup[Constants.MAP_SIDE_GRID_LEN][Constants.MAP_SIDE_GRID_LEN];
        Texture texture = Res.ROLE.get();
        role = new Image(texture);
        role.setSize(singleLen,singleLen);
    }

    /**
     * 按键事件监听处理
     * @param keyCode 键
     */
    public void moveAction(int keyCode){
        DataHolder dataHolder = GameContext.getScreenHandle().getDataHolder();
        Archiving archiving = dataHolder.getArchiving();
        Pos pos = archiving.getPos();
        Pos newPos = null;
        switch (keyCode){
            case Input.Keys.LEFT:
                newPos = new Pos(pos.getX() - 1,pos.getY());
                break;
            case Input.Keys.RIGHT:
                newPos = new Pos(pos.getX() + 1,pos.getY());
                break;
            case Input.Keys.UP:
                newPos = new Pos(pos.getX(),pos.getY() + 1);
                break;
            case Input.Keys.DOWN:
                newPos = new Pos(pos.getX(),pos.getY()-1);
                break;
        }
        if(newPos != null){
            if(newPos.getX() < 0 || newPos.getX() >= Constants.MAP_SIDE_GRID_LEN || newPos.getY() < 0
                    || newPos.getY() >= Constants.MAP_SIDE_GRID_LEN){
                return;
            }
            MoveResult moveResult = dataHolder.moveOn(newPos);
            if(!moveResult.isResult() && moveResult.getMsg() != null){
                Gdx.app.error("move",moveResult.getMsg());
            }
        }
    }

    /**
     * 数据全同步到界面
     */
    public void syncAllMapData(){
        elementGroups = new ElementGroup[Constants.MAP_SIDE_GRID_LEN][Constants.MAP_SIDE_GRID_LEN];
        this.clearChildren();
        DataHolder dataHolder = GameContext.getScreenHandle().getDataHolder();
        Archiving archiving = dataHolder.getArchiving();
        MapModel mapModel = archiving.getMapModelMap().get(archiving.getTowerId());
        if(mapModel == null){
            Gdx.app.error("map","地图数据不存在[" + archiving.getTowerId() + "]");
        }
        Element[][] elements = mapModel.getElements();
        for (int x = 0; x < elements.length; x++) {
            Element[] elementArray = elements[x];
            for (int y = 0; y < elementArray.length; y++) {
                Element element = elementArray[y];
                if(element != null){
                    ElementGroup elementGroup = new ElementGroup(element,singleLen);
                    elementGroup.setPosition(x * singleLen,y * singleLen);
                    elementGroups[x][y] = elementGroup;
                    addActor(elementGroup);
                    if(element.type() == ElementType.MONSTER){
                        //怪物预显示血量消耗
                        elementGroup.reloadHpCost();
                    }
                }
            }
        }
        role.setPosition(archiving.getPos().getX() * singleLen,archiving.getPos().getY() * singleLen);
        addActor(role);

    }

    /**
     * 移动后同步数据
     * @param newPos 新坐标
     */
    public void syncData(Pos newPos){
        role.setPosition(newPos.getX() * singleLen,newPos.getY() * singleLen);
        ElementGroup elementGroup = elementGroups[newPos.getX()][newPos.getY()];
        if(elementGroup != null){
            removeActor(elementGroup);
            elementGroups[newPos.getX()][newPos.getY()] = null;
        }
    }

    /**
     * 重新加载血量消耗
     */
    public void reloadHpCost(){
        for (ElementGroup[] elementGroup : elementGroups) {
            for (ElementGroup group : elementGroup) {
                if(group != null){
                    group.reloadHpCost();
                }
            }
        }
    }
}
