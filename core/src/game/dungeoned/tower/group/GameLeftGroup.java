package game.dungeoned.tower.group;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import game.dungeoned.tower.GameContext;
import game.dungeoned.tower.data.DataHolder;
import game.dungeoned.tower.data.archiving.Archiving;
import game.dungeoned.tower.data.archiving.Atag;
import game.dungeoned.tower.data.archiving.Key;
import game.dungeoned.tower.res.Res;

import static game.dungeoned.tower.Constants.LeftPanel;

public class GameLeftGroup extends BaseGroup {
    /** 当前层数 */
    private final Label towerId;
    /** 血量 */
    private final Label hpLabel;
    /** 血量上限 */
    private final Label hpMaxLabel;
    /** 攻击力 */
    private final Label atkLabel;
    /** 防御力 */
    private final Label defLabel;
    /** 蓝钥匙数量 */
    private final Label blueKeyLabel;
    /** 红钥匙数量 */
    private final Label redKeyLabel;
    /** 黄钥匙数量 */
    private final Label yellowKeyLabel;
    /** 金币数量 */
    private final Label goldLabel;

    public GameLeftGroup(float width, float height){
        super(width,height);
        BitmapFont bitmapFont = Res.CUSTOM_FONT.get();
        bitmapFont.getData().scale(-0.10f);
        Label.LabelStyle style = new Label.LabelStyle(bitmapFont,null);
        //塔层数
        towerId = new Label(String.format(LeftPanel.TEMF,0),style);
        towerId.setSize(towerId.getPrefWidth(),towerId.getPrefHeight());
        towerId.setPosition(getWidth()/2 - towerId.getWidth()/2, getHeight() - LeftPanel.LABEL_PAD * 3);
        towerId.setFontScale(1.2f,0.7f);
        addActor(towerId);
        //血量上限图标
        Texture textureHPMAX = Res.HP_MAX.get();
        Image hpMaxImg = new Image(textureHPMAX);
        hpMaxImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, towerId.getY() - towerId.getHeight() - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(hpMaxImg);
        //血量图标
        Texture textureHp = Res.HP.get();
        Image hpImg = new Image(textureHp);
        hpImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE,hpMaxImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(hpImg);
        //攻击力图标
        Texture textureATK = Res.ATK.get();
        Image atkImg = new Image(textureATK);
        atkImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, hpImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(atkImg);
        //防御力图标
        Texture textureDEF = Res.DEF.get();
        Image defImg = new Image(textureDEF);
        defImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, atkImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(defImg);
        //金币图标
        Texture textureGOLD = Res.GOLD.get();
        Image goldImg = new Image(textureGOLD);
        goldImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, defImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(goldImg);
        //红钥匙图标
        Texture textureRED = Res.RED_KEY.get();
        Image redKeyImg = new Image(textureRED);
        redKeyImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, goldImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(redKeyImg);
        //蓝钥匙图标
        Texture textureBLUE = Res.BLUE_KEY.get();
        Image blueKeyImg = new Image(textureBLUE);
        blueKeyImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, redKeyImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(blueKeyImg);
        //黄钥匙图标
        Texture textureYELLOW = Res.YELLOW_KEY.get();
        Image yellowKeyImg = new Image(textureYELLOW);
        yellowKeyImg.setBounds(getWidth()/2 - LeftPanel.IMG_SIZE, blueKeyImg.getY() - LeftPanel.IMG_SIZE - LeftPanel.LABEL_PAD
                ,LeftPanel.IMG_SIZE,LeftPanel.IMG_SIZE);
        addActor(yellowKeyImg);

        //血量
        hpLabel = new Label("0",style);
        hpLabel.setSize(hpLabel.getPrefWidth(),hpLabel.getPrefHeight());
        hpLabel.setPosition(hpImg.getX() + hpImg.getWidth() + LeftPanel.LABEL_PAD, hpImg.getY());
        addActor(hpLabel);
        //血量上限
        hpMaxLabel = new Label("0",style);
        hpMaxLabel.setSize(hpMaxLabel.getPrefWidth(),hpMaxLabel.getPrefHeight());
        hpMaxLabel.setPosition(hpMaxImg.getX() + hpMaxImg.getWidth() + LeftPanel.LABEL_PAD, hpMaxImg.getY());
        addActor(hpMaxLabel);
        //攻击力
        atkLabel = new Label("0",style);
        atkLabel.setSize(atkLabel.getPrefWidth(),atkLabel.getPrefHeight());
        atkLabel.setPosition(atkImg.getX() + atkImg.getWidth() + LeftPanel.LABEL_PAD, atkImg.getY());
        addActor(atkLabel);
        //防御力
        defLabel = new Label("0",style);
        defLabel.setSize(defLabel.getPrefWidth(),defLabel.getPrefHeight());
        defLabel.setPosition(defImg.getX() + defImg.getWidth() + LeftPanel.LABEL_PAD, defImg.getY());
        addActor(defLabel);
        //金币
        goldLabel = new Label("0",style);
        goldLabel.setSize(goldLabel.getPrefWidth(),goldLabel.getPrefHeight());
        goldLabel.setPosition(goldImg.getX() + goldImg.getWidth() + LeftPanel.LABEL_PAD, goldImg.getY());
        addActor(goldLabel);
        //红钥匙
        redKeyLabel = new Label("0",style);
        redKeyLabel.setSize(redKeyLabel.getPrefWidth(),redKeyLabel.getPrefHeight());
        redKeyLabel.setPosition(redKeyImg.getX() + redKeyImg.getWidth()  + LeftPanel.LABEL_PAD, redKeyImg.getY());
        addActor(redKeyLabel);
        //蓝钥匙
        blueKeyLabel = new Label("0",style);
        blueKeyLabel.setSize(blueKeyLabel.getPrefWidth(),blueKeyLabel.getPrefHeight());
        blueKeyLabel.setPosition(blueKeyImg.getX() + blueKeyImg.getWidth() + LeftPanel.LABEL_PAD, blueKeyImg.getY());
        addActor(blueKeyLabel);
        //蓝钥匙
        yellowKeyLabel = new Label("0",style);
        yellowKeyLabel.setSize(yellowKeyLabel.getPrefWidth(),yellowKeyLabel.getPrefHeight());
        yellowKeyLabel.setPosition(yellowKeyImg.getX() + yellowKeyImg.getWidth() +  LeftPanel.LABEL_PAD, yellowKeyImg.getY());
        addActor(yellowKeyLabel);
    }

    /**
     * 同步数据到界面
     */
    public void syncDataView(){
        DataHolder dataHolder = GameContext.getScreenHandle().getDataHolder();
        Archiving archiving = dataHolder.getArchiving();
        // 重新设置文本后, 文本的宽度可能被改变, 需要重新设置标签的宽度和X坐标

        //塔层数
        towerId.setText(String.format(LeftPanel.TEMF,archiving.getTowerId()));
        towerId.setWidth(towerId.getPrefWidth());
        //血量
        hpLabel.setText(archiving.getAttribute().getAttr(Atag.HP));
        hpLabel.setWidth(hpLabel.getPrefWidth());
        //血量上限
        hpMaxLabel.setText(archiving.getAttribute().getAttr(Atag.HPMAX));
        hpMaxLabel.setWidth(hpMaxLabel.getPrefWidth());
        //攻击力
        atkLabel.setText(archiving.getAttribute().getAttr(Atag.ATK));
        atkLabel.setWidth(atkLabel.getPrefWidth());
        //防御力
        defLabel.setText(archiving.getAttribute().getAttr(Atag.DEF));
        defLabel.setWidth(defLabel.getPrefWidth());
        //金币
        goldLabel.setText(archiving.getGold());
        goldLabel.setWidth(goldLabel.getPrefWidth());
        //红钥匙
        redKeyLabel.setText(archiving.getKeyNum(Key.RED));
        redKeyLabel.setWidth(redKeyLabel.getPrefWidth());
        //蓝钥匙
        blueKeyLabel.setText(archiving.getKeyNum(Key.BLUE));
        blueKeyLabel.setWidth(blueKeyLabel.getPrefWidth());
        //黄钥匙
        yellowKeyLabel.setText(archiving.getKeyNum(Key.YELLOW));
        yellowKeyLabel.setWidth(yellowKeyLabel.getPrefWidth());
    }



}
